class Coffee {
  constructor(nameType) {
    this.nameType = nameType;
  }
}
class Builder {
  constructor(nameType) {
    this.createCoffee = new Coffee(nameType);
  }

  syrup() {
    this.createCoffee.addChocolateSyrup = true;
    return this;
  }
  milk() {
    this.createCoffee.addMilk = true;
    return this;
  }
  cream() {
    this.createCoffee.addWhipedCream = true;
    return this;
  }
  milkFoam() {
    this.createCoffee.addMilkFoam = true;
    return this;
  }
  espresso() {
    this.createCoffee.addEspresso = true;
    return this;
  }

  build() {
    return this.createCoffee;
  }
}

const espresso = new Builder('Espresso').espresso().build();
const latte = new Builder('Latte').espresso().milk().milkFoam().build();
const moccha = new Builder('Moccha').espresso().syrup().milk().cream().build();
const macchiato = new Builder('Macchiato').espresso().milkFoam().build();
console.log(espresso);
console.log(latte);
console.log(moccha);
console.log(macchiato);
