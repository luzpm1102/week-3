import { api } from '../call.js';

const getUsers = async () => {
  const data = await api.getData('users');
  return data;
};

const getUserName = async (id) => {
  const data = await getUsers();
  const users = data.filter((user) => user.id === id);
  const usersNames = users
    .map((user) => user.name + ' ' + user.lastName)
    .join('');

  return usersNames;
};

export { getUserName, getUsers };
