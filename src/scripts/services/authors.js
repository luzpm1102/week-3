import { api } from '../call.js';

const getAuthors = async () => {
  const data = await api.getData('authors');
  return data;
};

export { getAuthors };
