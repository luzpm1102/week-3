//get tags

import { api } from '../call.js';

const getTags = async () => {
  const tags = await api.getData('tags');
  return tags;
};

export { getTags };
