import { filterPosts, filterRemainingPosts } from '../../helpers/helper.js';
import { api } from '../call.js';

//get post by tags

const getPostByTags = async (tags) => {
  try {
    const data = await api.getData('posts');
    const filtered = filterPosts(data, tags);
    return filtered;
  } catch (err) {
    console.log(err);
  }
};

//get post by word

const getPostByTitle = async (title) => {
  const data = await api.getData('posts');
  if (data) {
    const filtered = data.filter((post) => {
      if (post.title.toLowerCase().includes(title.toLowerCase())) {
        return post;
      }
    });
    return filtered;
  }
};

//get the last 3 post
const getLastPosts = await api.getData(
  'posts?_sort=createDate&_order=desc&_limit=3'
);

//Get remaining post not includes last post

const getRemainingPosts = async () => {
  const data = await api.getData('posts?_sort=createDate&_order=desc');
  const filtered = filterRemainingPosts(data);

  return filtered;
};

const getOnePost = async (id) => {
  const data = await api.getData(`posts/${id}`);
  return data;
};

//Post order desc

const getPostsOrdered = await api.getData('posts?_sort=createDate&_order=desc');

//View Post

const viewPost = async (id) => {
  location.assign('./src/pages/postView.html');
  const data = await getOnePost(id);
  return data;
};
//add new post

const createPostRedirect = () => {
  location.assign('./src/pages/createEditPost.html');
};

const createPost = async (post) => {
  await api.postData('posts', post).then((resp) => console.log(resp));
};

//edit post

const editData = async (id, body) => {
  await api.patchData('posts', id, body).then((res) => console.log(res));
};

//delete post

const deletePost = async (id) => {
  await api.deleteData('posts', id).then((resp) => console.log(resp));
};

export {
  getLastPosts,
  getPostByTags,
  getPostByTitle,
  getRemainingPosts,
  getPostsOrdered,
  getOnePost,
  createPostRedirect,
  createPost,
  viewPost,
  deletePost,
  editData,
};
