import { api } from '../call.js';

const getComments = async () => {
  const data = await api.getData('comments');
  return data;
};

const getCommentsFiltered = async (id) => {
  const data = await getComments();
  const comments = data.filter((comment) => comment.postId === Number(id));
  return comments;
};

const postComment = async (body) => {
  await api.postData('comments', body).then((res) => console.log(res));
};
export { getComments, getCommentsFiltered, postComment };
