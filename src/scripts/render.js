import { convertComments, getInfo } from '../helpers/helper.js';
import { commentsSection, lastPosts, menu } from './domElements.js';
import { getCommentsFiltered } from './services/comments.js';
import {
  getPostByTags,
  getLastPosts,
  getPostByTitle,
  getRemainingPosts,
} from './services/post.js';

const setTags = async (tags) => {
  const newTags = tags
    .map((tag) => {
      return `<li id=${tag.id} class='tags'>${tag.name}</li>`;
    })
    .join('');
  menu.innerHTML = newTags;
  return newTags;
};

const filterByTagsPost = async (tagsID) => {
  const post = await getPostByTags(tagsID);
  const data = await getInfo(post);
  remainingPost.innerHTML = data;
};

const fillPosts = async () => {
  const post = await getLastPosts;
  const data = await getInfo(post);
  lastPosts.innerHTML = data;
};
const fillRemainingPosts = async () => {
  const post = await getRemainingPosts();
  const data = await getInfo(post);
  remainingPost.innerHTML = data;
};
const showPostByTitles = async (e) => {
  let data;
  const post = await getPostByTitle(e.target.value);
  data = await getInfo(post);
  if (post.length === 0) {
    data = `<h2>Not found</h2>`;
  }
  remainingPost.innerHTML = data;
};
const showPostByword = async (word) => {
  let data;
  const post = await getPostByTitle(word);
  data = await getInfo(post);
  if (post.length === 0) {
    data = `<h2>Not found</h2>`;
  }
  remainingPost.innerHTML = data;
};

const fillComments = async (id) => {
  const data = await getCommentsFiltered(id);
  const comments = await convertComments(data);

  commentsSection.innerHTML = comments;
};

export {
  setTags,
  fillPosts,
  fillRemainingPosts,
  filterByTagsPost,
  showPostByTitles,
  fillComments,
  showPostByword,
};
