//Singleton pattern

let instance = null;

class RequestApi {
  constructor(URL) {
    if (!instance) {
      instance = this;
    }

    this.url = 'https://server-week-3-lcpm.herokuapp.com/';
  }

  async getData(whatToGet = '') {
    const data = await fetch(`${this.url}${whatToGet}`)
      .then((res) => {
        if (!res.ok) {
          throw Error(res.statusText);
        }
        return res.json();
      })
      .catch((error) => {
        console.log(error);
      });

    return data;
  }
  async postData(whatToPost = '', data = {}) {
    const res = await fetch(`${this.url}${whatToPost}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then((res) => {
        if (!res.ok) {
          throw Error(res.statusText);
        }
        return res.json();
      })
      .catch((error) => {
        console.log(error);
      });

    return res;
  }

  patchData(whatToPatch, id, body) {
    const response = fetch(`${this.url}${whatToPatch}/${id}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: JSON.stringify(body),
    })
      .then((resp) => resp)
      .catch((err) => console.log(err));

    return response;
  }

  deleteData(whatToDelete, id) {
    const response = fetch(`${this.url}${whatToDelete}/${id}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    })
      .then((res) => res)
      .catch((err) => console.log(err));

    return response;
  }
}

export { RequestApi };
