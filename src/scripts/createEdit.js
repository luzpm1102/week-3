import { goBack, setAuthors } from '../helpers/helper.js';
import {
  checkInputNotEmpty,
  errorsCount,
  fillAuthors,
  getDataValues,
  getDate,
  postObject,
  tagsValidation,
} from '../helpers/formHelpers.js';
import {
  author,
  date,
  description,
  form,
  goBackButton,
  image,
  pageTitle,
  submit,
  subtitle,
  tags,
  title,
} from './domElements.js';
import { createPost, editData } from './services/post.js';

let errors;
const urlParams = new URLSearchParams(window.location.search);
const postId = urlParams.get('id');

if (postId) {
  getDataValues(postId);
  submit.innerText = 'UPDATE';
  pageTitle.innerText = 'UPDATE POST ';
}

form.addEventListener('submit', async (e) => {
  e.preventDefault();
  errors = document.querySelectorAll('.form--small');
  await validations();

  if (errorsCount(errors) > 0) {
    return;
  }

  if (!postId) {
    sendData(postObject());
    return;
  }
  updateData(postId, postObject());
});

const sendData = async (body) => {
  await createPost(body);
  form.reset();
  getDate();
  alert('created');
  goBack();
};

const updateData = async (id, post) => {
  await editData(id, post);
  alert('Updated');
  goBack();
};

const validations = async () => {
  checkInputNotEmpty(date);
  checkInputNotEmpty(title);
  checkInputNotEmpty(subtitle);
  checkInputNotEmpty(image);
  checkInputNotEmpty(description);
  //   checkInputNotEmpty(tags);
  await tagsValidation(tags);
  checkInputNotEmpty(author);
};
goBackButton.addEventListener('click', () => goBack());

// getDate();

fillAuthors();

getDate();
