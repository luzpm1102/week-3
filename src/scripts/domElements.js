const menu = document.getElementById('menu');
const lastPosts = document.getElementById('lastPosts');
const remainingPost = document.getElementById('remainingPost');
const searchInput = document.getElementById('searchValue');
const createButton = document.getElementById('create');
const feature = document.getElementById('feature');
const searchButton = document.getElementById('search');
let tagsID = [];

//view post html
const postDiv = document.getElementById('post');
const goBackButton = document.getElementById('goBack');
const editButton = document.getElementById('edit');
const deleteButton = document.getElementById('delete');
const commentsSection = document.getElementById('comments');
const send = document.getElementById('send');
const user = document.getElementById('user');
const userComment = document.getElementById('userComment');
//create/edit html

const date = document.getElementById('createDate');
const title = document.getElementById('title');
const subtitle = document.getElementById('subTitle');
const description = document.getElementById('body');
const author = document.getElementById('author');
const tags = document.getElementById('tags');
const submit = document.getElementById('submit');
const form = document.getElementById('form');
const image = document.getElementById('image');
const pageTitle = document.getElementById('pageTitle');

export {
  menu,
  tagsID,
  lastPosts,
  feature,
  remainingPost,
  searchInput,
  createButton,
  searchButton,
  postDiv,
  goBackButton,
  editButton,
  deleteButton,
  title,
  subtitle,
  description,
  author,
  tags,
  pageTitle,
  submit,
  date,
  form,
  image,
  commentsSection,
  send,
  user,
  userComment,
};
