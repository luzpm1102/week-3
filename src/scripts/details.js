import { getInfoForOne, goBack } from '../helpers/helper.js';
import { fillUsers } from '../helpers/formHelpers.js';
import {
  deleteButton,
  editButton,
  goBackButton,
  postDiv,
  send,
  user,
  userComment,
} from './domElements.js';
import { fillComments } from './render.js';
import { getCommentsFiltered, postComment } from './services/comments.js';
import { deletePost, getOnePost } from './services/post.js';

const urlParams = new URLSearchParams(window.location.search);
const postId = urlParams.get('id');
const commentObject = new Object();
const showPost = async (id) => {
  const data = await getOnePost(id);
  const post = await getInfoForOne(data);

  postDiv.innerHTML = post;
};
goBackButton.addEventListener('click', () => goBack());
deleteButton.addEventListener('click', () => deleteP());
editButton.addEventListener('click', () => editRedirection(postId));
send.addEventListener('click', () => sendComment());

const sendComment = () => {
  getValues();
  postComment(commentObject);
  alert('comment added');
  location.reload();
};
const deleteP = () => {
  if (confirm('Are you sure you want to delete this post?')) {
    deletePost(postId);
    alert('deleted');
    goBack();
  }
};

const editRedirection = (id) => {
  location.assign(`../pages/createEditPost.html?id=${id}`);
};

const getValues = () => {
  const comment = userComment.value.trim();
  const userId = user.value;
  if (!comment) {
    alert('comment required');
    return;
  }
  if (!userId) {
    alert('user required');
    return;
  }
  insertInObject('comment', comment);
  insertInObject('user', Number(userId));
  insertInObject('postId', Number(postId));
};

const insertInObject = (id, value) => {
  commentObject[id] = value;
};
showPost(postId);
getCommentsFiltered(postId);
fillComments(postId);
fillUsers();
