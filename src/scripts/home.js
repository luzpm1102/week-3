import { debounce } from '../helpers/debounce.js';
import { throttle } from '../helpers/throttle.js';
import {
  createButton,
  lastPosts,
  searchButton,
  searchInput,
  tagsID,
} from './domElements.js';
import {
  fillPosts,
  fillRemainingPosts,
  filterByTagsPost,
  setTags,
  showPostByTitles,
  showPostByword,
} from './render.js';
import { createPostRedirect } from './services/post.js';

import { getTags } from './services/tags.js';

const tagsView = async () => {
  try {
    const tags = await getTags();
    await setTags(tags);
    tagListener();
  } catch (error) {
    console.log(error);
  }
};

const tagListener = () => {
  const tags = document.querySelectorAll('.tags');
  tags.forEach((tag) => {
    tag.addEventListener('click', (e) => {
      selectedTags(Number(e.currentTarget.id));
      if (!tagsID.length) {
        fillRemainingPosts();
        fillPosts();
        return;
      }
      lastPosts.innerHTML = '';
      filterByTagsPost(tagsID);
    });
  });
};

const searchInputListener = () => {
  searchInput.addEventListener(
    'input',
    debounce((e) => {
      showTitles(e);
    })
  );
};
const showTitles = (e) => {
  lastPosts.innerHTML = '';
  showPostByTitles(e);
};
const searchByWord = (word) => {
  lastPosts.innerHTML = '';
  showPostByword(word);
};

const searchButtonListener = () => {
  searchButton.addEventListener(
    'click',
    throttle(() => {
      searchByWord(searchInput.value);
    })
  );
};

const createButtonListener = () => {
  createButton.addEventListener('click', () => {
    createPostRedirect();
  });
};

const selectedTags = (id) => {
  let selected = document.getElementById(id.toString());

  if (!tagsID.includes(id)) {
    tagsID.push(id);
    selected.classList.add('selected');
  } else {
    for (let i = 0; i < tagsID.length; i++) {
      if (tagsID[i] == id) {
        tagsID.splice(i, 1);
        selected.classList.remove('selected');
      }
    }
  }
};

tagsView();
fillPosts();
fillRemainingPosts();
searchInputListener();
createButtonListener();
searchButtonListener();
