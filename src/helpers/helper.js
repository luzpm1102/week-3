import { api } from '../scripts/call.js';
import { getLastPosts } from '../scripts/services/post.js';
import { getUserName } from '../scripts/services/users.js';

const card = (
  image,
  title,
  subtitle,
  createDate,
  body,
  author,
  tag,
  id,
  likes
) => {
  return `<a href='./src/pages/postView.html?id=${id}' class='card--link'><div class="post" id=${id}>
          <div class="post--header">
            <img src="${image}" />
          </div>
          <div class="post--body">
            ${tag}
            
            <h3>${title}</h3>
            <div class='post--line'></div>
            <h4>${subtitle}</h4>
            <p>${ellipsify(body)}</p>
          </div>
          <div class="post--footer">
           <h5>${author}</h5>
            <small>${createDate}</small>
            <div class='post--likes'>
              <img src="../../src/img/full.png" >
              <small>${likes}</small>
            </div>
          </div>
        </div></a>`;
};

const detail = (
  image,
  title,
  subtitle,
  createDate,
  body,
  author,
  tag,
  id,
  likes
) => {
  return `<div class="post" id=${id}>
          <div class="post--header">
            <img src="${image}" />
          </div>
          <div class="post--body">
            <div class='post--section'>
              <div class='post--tags'>${tag}</div>
              <div class='post--likes' id='postLikes'>
              <img src="../../src/img/full.png" >
               <small class='post--likes'>${likes}</small>
              </div>
            </div>
            <h3>${title}</h3>
            <hr />
            <h4>${subtitle}</h4>
            <p>${body}</p>
          </div>
          <div class="post--footer">
           <h5>${author}</h5>
            <small>${createDate}</small>
          </div>
        </div>`;
};

const ellipsify = (str) => {
  if (str.length > 10) {
    return str.substring(0, 55) + '...';
  } else {
    return str;
  }
};

const filterPosts = (data, tags) => {
  let filtered = [];
  data.filter((post) => {
    tags.forEach((tag) => {
      if (post.tags.indexOf(tag) !== -1) {
        if (!filtered.includes(post)) {
          filtered.push(post);
        }
      }
    });
  });
  return filtered;
};

const lastPostId = getLastPosts.map((last) => last.id);

const filterRemainingPosts = (data) => {
  const filtered = data.filter((post) => {
    return !lastPostId.includes(post.id);
  });

  return filtered;
};

//get complete posts info

const getInfo = async (posts) => {
  const authors = await api.getData('authors');
  const tags = await api.getData('tags');
  const data = posts
    .map((post) => {
      const tag = post.tags
        .map((tag) => {
          const nameTags = tags.filter((apiTag) => tag === apiTag.id);
          return `<span>${nameTags[0].name}</span>`;
        })
        .join(' ');

      let authorData = authors.filter((author) => post.author === author.id);
      let author = authorData.map(
        (author) => author.name + ' ' + author.lastName
      );

      return card(
        post.image,
        post.title,
        post.subTitle,
        post.createDate,
        post.body,
        author,
        tag,
        post.id,
        post.likes
      );
    })
    .join('');
  return data;
};

const getInfoForOne = async (post) => {
  const authors = await api.getData('authors');
  const tags = await api.getData('tags');

  let authorData = authors.filter((author) => post.author === author.id);
  let author = authorData[0].name + ' ' + authorData[0].lastName;

  const tag = post.tags
    .map((tag) => {
      const nameTags = tags.filter((apiTag) => tag === apiTag.id);
      return `<span>${nameTags[0].name}</span>`;
    })
    .join(' ');

  return detail(
    post.image,
    post.title,
    post.subTitle,
    post.createDate,
    post.body,
    author,
    tag,
    post.id,
    post.likes
  );
};

const setAuthors = (authors) => {
  const options = authors.map((author) => {
    let option = document.createElement('option');

    option.value = author.id;
    option.innerText = author.name + ' ' + author.lastName;
    return option;
  });
  return options;
};
const goBack = () => {
  location.assign('../../index.html');
};

const convertComments = (data) => {
  const comments = data.map(async (comment) => {
    let transformed = commentCard(
      await getUserName(comment.user),
      comment.comment
    );
    return transformed;
  });

  return Promise.all(comments);
};

const commentCard = (user, body) => {
  const comment = `<div class='comment'>
  <h4>${user}:</h4>
  <p>${body}</p>
  </div>`;

  return comment;
};
export {
  card,
  ellipsify,
  filterPosts,
  filterRemainingPosts,
  getInfo,
  getInfoForOne,
  detail,
  setAuthors,
  goBack,
  convertComments,
};
