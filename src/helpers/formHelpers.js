import {
  author,
  date,
  description,
  image,
  subtitle,
  tags,
  title,
} from '../scripts/domElements.js';
import { getTags } from '../scripts/services/tags.js';
import { getAuthors } from '../scripts/services/authors.js';
import { getOnePost } from '../scripts/services/post.js';
import { setAuthors } from './helper.js';
import { getUsers } from '../scripts/services/users.js';

const post = new Object();
post.likes = 0;

//for validations on create post
const checkInputNotEmpty = (input) => {
  const inputValue = input.value.trim();
  if (inputValue === '') {
    setError(input);
    return;
  }
  setSuccess(input);
  if (input.id === 'author') {
    insertInObject(input.id, Number(inputValue));
    return;
  }

  insertInObject(input.id, inputValue);
};

const convertTags = (input) => {
  const tagsValues = input.value;
  const tagsArray = tagsValues.split(',');
  const tags = tagsArray.map((tag) => tag.toLowerCase());
  return tags;
};

const setTagValues = async (tags) => {
  const allTags = await getTags();

  const tagName = tags.map((tag) => {
    for (let i = 0; i < allTags.length; i++) {
      if (allTags[i].id === tag) {
        return allTags[i].name;
      }
    }
  });
  return tagName;
};

const tagsValidation = async (input) => {
  if (!input.value) {
    setError(input);
    return;
  }
  const tags = convertTags(input);
  const existingTags = await getTags();
  const valids = [];
  tags.forEach((tag) => {
    existingTags.forEach((existing) => {
      if (!existing.slug.includes(tag)) {
        return;
      }
      valids.push(existing.id);
    });
  });

  if (valids.length !== tags.length) {
    setError(input, 'doesnt exist');
    return;
  }

  setSuccess(input);
  insertInObject(input.id, valids);
};

const setError = (input, message) => {
  const small = document.getElementById(`${input.id}Error`);
  small.classList.add('error');
  if (message) {
    small.innerText = `${input.id} ${message}`;
    return;
  }
  small.innerText = `${input.id} can't be empty`;
};

const setSuccess = (input) => {
  const small = document.getElementById(`${input.id}Error`);
  small.classList.add('success');
};

const getDate = () => {
  const day = new Date();
  const today = `${day.getFullYear()}/${day.getMonth() + 1}/${day.getDate()}`;

  date.value = today;
};
const errorsCount = (errors) => {
  let counter = 0;
  errors.forEach((error) => {
    if (error.className === 'form--small error') {
      counter++;
    }
  });
  return counter;
};

const insertInObject = (id, value) => {
  post[id] = value;
};

const postObject = () => {
  return post;
};

const getDataValues = async (id) => {
  const data = await getOnePost(id);
  date.value = data.createDate;
  title.value = data.title;
  subtitle.value = data.subTitle;
  description.value = data.body;
  image.value = data.image;
  author.value = data.author;
  tags.value = await setTagValues(data.tags);
  post.likes = data.likes;
};
const fillAuthors = async () => {
  const authors = await getAuthors();
  const options = await setAuthors(authors);
  options.forEach((option) => {
    document.querySelector('#author').appendChild(option);
  });
};
const fillUsers = async () => {
  const users = await getUsers();
  const options = await setAuthors(users);
  options.forEach((option) => {
    document.querySelector('#user').appendChild(option);
  });
};

export {
  checkInputNotEmpty,
  setError,
  setSuccess,
  getDate,
  errorsCount,
  postObject,
  tagsValidation,
  getDataValues,
  fillAuthors,
  fillUsers,
};
